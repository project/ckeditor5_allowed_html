<?php

namespace Drupal\ckeditor5_allowed_html\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter to allow html.
 *
 * @Filter(
 *   id = "filter_allowed",
 *   title = @Translation("Limit allowed HTML tags and correct faulty HTML - Editable tag list"),
 *   description = @Translation("Allows extra HTML tags that are not part of the standard CkEditor 5 list.."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE
 * )
 */
class FilterAllowed extends FilterBase {

  /**
   * The processed HTML restrictions.
   *
   * @var array
   */
  protected $restrictions;

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
      if (empty($this->settings['allowed_html'])) {
          $this->settings['allowed_html'] = '<br> <p> <h2> <h3> <h4> <h5> <h6> <strong> <em> <sup> <blockquote> <a href> <ul> <ol start> <li> <table> <tr> <td rowspan colspan> <th rowspan colspan> <thead> <tbody> <tfoot> <caption> <drupal-media data-entity-type data-entity-uuid alt>';
      }

    $form['allowed_html'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Allowed HTML tags'),
      '#default_value' => $this->settings['allowed_html'],
      '#description' => $this->t('A list of HTML tags that can be used. By default only the <em>lang</em> and <em>dir</em> attributes are allowed for all HTML tags. Each HTML tag may have attributes which are treated as allowed attribute names for that HTML tag. Each attribute may allow all values, or only allow specific values. Attribute names or values may be written as a prefix and wildcard like <em>jump-*</em>. JavaScript event attributes, JavaScript URLs, and CSS are always stripped.<p><b>NOTE that by installing this filter and editing this list you are taking responsibility for manually including <i>all</i> HTML tags that are required.</b></p>'),
      '#attached' => [
        'library' => [
          'filter/drupal.filter.filter_html.admin',
        ],
      ],
    ];
    $form['filter_html_help'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display basic HTML help in long filter tips'),
      '#default_value' => $this->settings['filter_html_help'],
    ];
    $form['filter_html_nofollow'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add rel="nofollow" to all links'),
      '#default_value' => $this->settings['filter_html_nofollow'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    if (isset($configuration['settings']['allowed_html'])) {
      // The javascript in core/modules/filter/filter.filter_html.admin.js
      // removes new lines and double spaces so, for consistency when javascript
      // is disabled, remove them.
      $configuration['settings']['allowed_html'] = preg_replace('/\s+/', ' ', $configuration['settings']['allowed_html']);
    }
    parent::setConfiguration($configuration);
    // Force restrictions to be calculated again.
    $this->restrictions = NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $restrictions = $this->getHtmlRestrictions();
    // Split the work into two parts. For filtering HTML tags out of the content
    // we rely on the well-tested Xss::filter() code. Since there is no '*' tag
    // that needs to be removed from the list.
    unset($restrictions['allowed']['*']);
    $text = Xss::filter($text, array_keys($restrictions['allowed']));
    // After we've done tag filtering, we do attribute and attribute value
    // filtering as the second part.
    return new FilterProcessResult($this->filterAttributes($text));
    //return new FilterProcessResult($text);
  }

  /**
   * Provides filtering of tag attributes into accepted HTML.
   *
   * @param string $text
   *   The HTML text string to be filtered.
   *
   * @return string
   *   Filtered HTML with attributes filtered according to the settings.
   */
  public function filterAttributes($text) {
    $restrictions = $this->getHTMLRestrictions();
    $global_allowed_attributes = array_filter($restrictions['allowed']['*']);
    unset($restrictions['allowed']['*']);

    // Apply attribute restrictions to tags.
    $html_dom = Html::load($text);
    $xpath = new \DOMXPath($html_dom);
    foreach ($restrictions['allowed'] as $allowed_tag => $tag_attributes) {
      // By default, no attributes are allowed for a tag, but due to the
      // globally allowed attributes, it is impossible for a tag to actually
      // completely disallow attributes.
      if ($tag_attributes === FALSE) {
        $tag_attributes = [];
      }
      $allowed_attributes = ['exact' => [], 'prefix' => []];
      foreach (($global_allowed_attributes + $tag_attributes) as $name => $values) {
        // A trailing * indicates wildcard, but it must have some prefix.
        if (substr($name, -1) === '*' && $name[0] !== '*') {
          $allowed_attributes['prefix'][str_replace('*', '', $name)] = $this->prepareAttributeValues($values);
        }
        else {
          $allowed_attributes['exact'][$name] = $this->prepareAttributeValues($values);
        }
      }
      krsort($allowed_attributes['prefix']);

      // Find all matching elements that have any attributes and filter the
      // attributes by name and value.
      foreach ($xpath->query('//' . $allowed_tag . '[@*]') as $element) {
        $this->filterElementAttributes($element, $allowed_attributes);
      }
    }

    if ($this->settings['filter_html_nofollow']) {
      $links = $html_dom->getElementsByTagName('a');
      foreach ($links as $link) {
        $link->setAttribute('rel', 'nofollow');
      }
    }
    $text = Html::serialize($html_dom);

    return trim($text);
  }

  /**
   * Helper function to prepare attribute values including wildcards.
   *
   * Splits the values into two lists, one for values that must match exactly
   * and the other for values that are wildcard prefixes.
   *
   * @param bool|array $attribute_values
   *   TRUE, FALSE, or an array of allowed values.
   *
   * @return bool|array
   */
  protected function prepareAttributeValues($attribute_values) {
    if ($attribute_values === TRUE || $attribute_values === FALSE) {
      return $attribute_values;
    }
    $result = ['exact' => [], 'prefix' => []];
    foreach ($attribute_values as $name => $allowed) {
      // A trailing * indicates wildcard, but it must have some prefix.
      if (substr($name, -1) === '*' && $name[0] !== '*') {
        $result['prefix'][str_replace('*', '', $name)] = $allowed;
      }
      else {
        $result['exact'][$name] = $allowed;
      }
    }
    krsort($result['prefix']);
    return $result;
  }

  /**
   * Filters attributes on an element according to a list of allowed values.
   *
   * @param \DOMElement $element
   *   The element to be processed.
   * @param array $allowed_attributes
   *   The list of allowed attributes as an array of names and values.
   */
  protected function filterElementAttributes(\DOMElement $element, array $allowed_attributes) {
    $modified_attributes = [];
    foreach ($element->attributes as $name => $attribute) {
      // Remove attributes not in the list of allowed attributes.
      $allowed_value = $this->findAllowedValue($allowed_attributes, $name);
      if (empty($allowed_value)) {
        $modified_attributes[$name] = FALSE;
      }
      elseif ($allowed_value !== TRUE) {
        // Check the list of allowed attribute values.
        $attribute_values = preg_split('/\s+/', $attribute->value, -1, PREG_SPLIT_NO_EMPTY);
        $modified_attributes[$name] = [];
        foreach ($attribute_values as $value) {
          if ($this->findAllowedValue($allowed_value, $value)) {
            $modified_attributes[$name][] = $value;
          }
        }
      }
    }
    // If the $allowed_value was TRUE for an attribute name, it does not
    // appear in this array so the value on the DOM element is left unchanged.
    foreach ($modified_attributes as $name => $values) {
      if ($values) {
        $element->setAttribute($name, implode(' ', $values));
      }
      else {
        $element->removeAttribute($name);
      }
    }
  }



  /**
   * {@inheritdoc}
   */
  public function getHTMLRestrictions() {
    if ($this->restrictions) {
      return $this->restrictions;
    }

    // Parse the allowed HTML setting, and gradually make the list of allowed
    // tags more specific.
    $restrictions = ['allowed' => []];

    // Make all the tags self-closing, so they will be parsed into direct
    // children of the body tag in the DomDocument.
    $html = str_replace('>', ' />', $this->settings['allowed_html']);
    // Protect any trailing * characters in attribute names, since DomDocument
    // strips them as invalid.
    // cSpell:disable-next-line
    $star_protector = '__zqh6vxfbk3cg__';
    $html = str_replace('*', $star_protector, $html);
    $body_child_nodes = Html::load($html)->getElementsByTagName('body')->item(0)->childNodes;

    foreach ($body_child_nodes as $node) {
      if ($node->nodeType !== XML_ELEMENT_NODE) {
        // Skip the empty text nodes inside tags.
        continue;
      }
      $tag = $node->tagName;

      // All attributes are already allowed on this tag, this is the most
      // permissive configuration, no additional processing is required.
      if (isset($restrictions['allowed'][$tag]) && $restrictions['allowed'][$tag] === TRUE) {
        continue;
      }

      if ($node->hasAttributes()) {
        // If the tag is not yet present, prepare to add attribute restrictions.
        // Otherwise, check if a more restrictive configuration (FALSE, meaning
        // no attributes were allowed) is present: then override the existing
        // value to prepare to add attribute restrictions.
        if (!isset($restrictions['allowed'][$tag]) || $restrictions['allowed'][$tag] === FALSE) {
          $restrictions['allowed'][$tag] = [];
        }

        // Iterate over any attributes, and mark them as allowed.
        foreach ($node->attributes as $name => $attribute) {
          // Only add specific attribute values if all values are not already
          // allowed.
          if (isset($restrictions['allowed'][$tag][$name]) && $restrictions['allowed'][$tag][$name] === TRUE) {
            continue;
          }
          // Put back any trailing * on wildcard attribute name.
          $name = str_replace($star_protector, '*', $name);

          // Put back any trailing * on wildcard attribute value and parse out
          // the allowed attribute values.
          $allowed_attribute_values = preg_split('/\s+/', str_replace($star_protector, '*', $attribute->value), -1, PREG_SPLIT_NO_EMPTY);

          // Sanitize the attribute value: it lists the allowed attribute values
          // but one allowed attribute value that some may be tempted to use
          // is specifically nonsensical: the asterisk. A prefix is required for
          // allowed attribute values with a wildcard. A wildcard by itself
          // would mean allowing all possible attribute values. But in that
          // case, one would not specify an attribute value at all.
          $allowed_attribute_values = array_filter($allowed_attribute_values, function ($value) {
            return $value !== '*';
          });

          if (empty($allowed_attribute_values)) {
            // If the value is the empty string all values are allowed.
            $restrictions['allowed'][$tag][$name] = TRUE;
          }
          else {
            // A non-empty attribute value is assigned, mark each of the
            // specified attribute values as allowed.
            foreach ($allowed_attribute_values as $value) {
              $restrictions['allowed'][$tag][$name][$value] = TRUE;
            }
          }
        }
      }

      if (empty($restrictions['allowed'][$tag])) {
        // Mark the tag as allowed, but with no attributes allowed.
        $restrictions['allowed'][$tag] = FALSE;
      }
    }

    // The 'style' and 'on*' ('onClick' etc.) attributes are always forbidden,
    // and are removed by Xss::filter().
    // The 'lang', and 'dir' attributes apply to all elements and are always
    // allowed. The list of allowed values for the 'dir' attribute is enforced
    // by self::filterAttributes(). Note that those two attributes are in the
    // short list of globally usable attributes in HTML5. They are always
    // allowed since the correct values of lang and dir may only be known to
    // the content author. Of the other global attributes, they are not usually
    // added by hand to content, and especially the class attribute can have
    // undesired visual effects by allowing content authors to apply any
    // available style, so specific values should be explicitly allowed.
    // @see http://www.w3.org/TR/html5/dom.html#global-attributes
    $restrictions['allowed']['*'] = [
      'style' => FALSE,
      'on*' => FALSE,
      'lang' => TRUE,
      'dir' => ['ltr' => TRUE, 'rtl' => TRUE],
    ];
    // Save this calculated result for re-use.
    $this->restrictions = $restrictions;

    return $restrictions;
  }

  /**
   * Helper function to handle prefix matching.
   *
   * @param array $allowed
   *   Array of allowed names and prefixes.
   * @param string $name
   *   The name to find or match against a prefix.
   *
   * @return bool|array
   */
  protected function findAllowedValue(array $allowed, $name) {
    if (isset($allowed['exact'][$name])) {
      return $allowed['exact'][$name];
    }
    // Handle prefix (wildcard) matches.
    foreach ($allowed['prefix'] as $prefix => $value) {
      if (str_starts_with($name, $prefix)) {
        return $value;
      }
    }
    return FALSE;
  }


}
